#ifndef _GSI_BLOCKS
#define _GSI_BLOCKS

typedef struct gsi_block *p_gsi_block;

struct gsi_block {
  char *word_index;
  char *add_info;
  char *data;
  p_gsi_block next;
};

p_gsi_block gb_alocar_gsi_block (const char *wi, const char *ai, const char *da,
				 const int gsi_format);

void gb_free_gsi_block (p_gsi_block block);

#endif
