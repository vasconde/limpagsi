/*
 * Nome:   limpaGSI
 * Desc:   Processa ficheiros no formato GSI
 * Autor:  Vasco Conde
 * Versão: 0.1 beta
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "gsi_blocks.h"


#define GSI_FORMAT 8
/* #define GSI_FORMAT 16 */

/*menu*/

int menu (void)
{
  int op;

  printf("\n");

  printf("1. Ler ficheiro\n");
  printf("2. Sobre\n");
  printf("3. Sair\n");
  printf("Op: ");

  scanf("%d", &op);

  printf("\n");
  
  return op;
}

/*
 * Apresenta na consola na info sobre a aplicacao
 */

void about (void)
{
  printf("Aplicação para tratamento de ficheiros GSI\n");
  printf("Produzida por Vasco Conde para LNEC\n");
  printf("Licença GPL v3\n");
  printf("Codigo disponivel em: ...\n");
}


/*
 * 
 */

void inputFileName(char inFileName[], char outFileName[])
{
  printf("Ficheiro de entrada: ");
  scanf("%s", inFileName);
  printf("Ficheiro de saida: ");
  scanf("%s", outFileName);
}


/*
 * parte um bloco GSI nas suas compoentes
 */
void breakGSIString (const char *data_element, char *p, char *s, char *t)
{
  int i;

  for(i=0; i < 3; i++)
    {
      p[i] = data_element[i];
    }
  p[i] = '\0';

  for(i=0; i < 3; i++)
    {
      s[i] = data_element[i+3];
    }
  s[i] = '\0';

  for(i=0; i < GSI_FORMAT+1; i++)
    {
      t[i] = data_element[i+6];
    }
  t[i] = '\0';
}


/*
 * retira o +00000 dos nomes dos pontos
 */
void trataNome(char *in, char *out)
{
  int i, j;

  for (i = 1; in[i] == '0'; i++)
    ;
      
  for (j = 0; i < GSI_FORMAT+1; i++, j++)
    out[j] = in[i];

  out[j] = '\0';
    
}

void gsi_block_2_csv(FILE *fout, p_gsi_block b)
{
  char p[GSI_FORMAT+1+1]; /*nome do ponto sem o +00000*/
  
  if( !strcmp (b->word_index, "110") )
    {
      trataNome(b->data,p); /*eh retirado o +0000 para apresentacao*/
      fprintf(fout, "%s", p);
    }
  else if( !strcmp (b->word_index, "21.") ) /*Hz*/
    {
      fprintf(fout, ",%d", atoi(b->data));
    }
  else if( !strcmp (b->word_index, "22.") ) /*Hz*/
    {
      fprintf(fout, ",%d\n", atoi(b->data));
    }
}

int readElements (FILE *fgsi, FILE *fout)
{
  char data_element[GSI_FORMAT+7+1];
  int i;

  char p[3+1];
  char s[3+1];
  char t[GSI_FORMAT+1+1];

  /* for(i=0; fscanf(fgsi, "%s", data_element) != EOF; i++) */
  /*   { */
  /*     fprintf(fout, "* %s *\n", data_element); */
  /*   } */

  /*for (i=0; i < 5; i++)*/
  p_gsi_block n;
  for(i=0; fscanf(fgsi, "%s", data_element) != EOF; i++)
    {
      /*le do ficheiro um bloco*/
      /*fscanf(fgsi, "%s", data_element);*/

      /*quebra o bloco nas treas componentes do formato gsi*/
      breakGSIString (data_element, p, s, t);

      /*aloca e guarda essas componentes na estrutura gsi_block*/
      n = gb_alocar_gsi_block (p, s, t, GSI_FORMAT);

      gsi_block_2_csv(fout, n);
      
      /*      printf("%s***%s***%s***\n", n->word_index, n->add_info, n->data); */

      /*liberta o gsi_block*/
      gb_free_gsi_block (n);


    }



  return i;
}


int main (void)
{
  int op;

  char inFileName[60] = "data/pol.gsi";
  char outFileName[60] = "data/res.csv";


  FILE *fgsi;
  FILE *fout;

  

  do {

    op = menu();

    switch(op)
      {
      case 1:
	/*entrada dos nomes dos ficheiros*/
	/*inputFileName(inFileName, outFileName);*/

	/*abrir ficheiros*/
	fgsi = fopen(inFileName, "r");
	fout = fopen(outFileName, "w");

	readElements (fgsi,fout);

	/*fechar ficheiros*/
	fclose(fgsi);
	fclose(fout);

	break;
      case 2:
	about();
	break;
      default:
	break;
      }
  
  } while(op != 3);
  
  return 0;

}
