
#include "gsi_blocks.h"

#include <stdlib.h>
#include <string.h>


p_gsi_block gb_alocar_gsi_block (const char *wi, const char *ai, const char *da,
				 const int gsi_format)
{

  p_gsi_block new_block = (p_gsi_block)malloc(sizeof(struct gsi_block));

  new_block->word_index = malloc( (3+1) * sizeof(char));
  new_block->add_info = malloc((3+1) * sizeof(char));
  new_block->data = malloc( (1+gsi_format+1) * sizeof(char));

  /*strcpy ( char * destination, const char * source );*/
  strcpy ( new_block->word_index, wi );
  strcpy ( new_block->add_info, ai );
  strcpy ( new_block->data, da );

  return new_block;

}

void gb_free_gsi_block (p_gsi_block block)
{
  free(block->word_index);
  free(block->add_info);
  free(block->data);

  free(block);
}
