# # # Makefle # # #
# na consola executar: make

CC = gcc
CSTD = -ansi
CFLAGS = -Wall -g
#CLIBS = -lm 

all:limpaGSI

limpaGSI: limpaGSI.o gsi_blocks.o
	$(CC) $(CFLAGS) $(CSTD) gsi_blocks.o limpaGSI.o -o limpaGSI

limpaGSI.o: limpaGSI.c
	$(CC) $(CFLAGS) $(CSTD) -c limpaGSI.c -o limpaGSI.o

gsi_blocks.o: gsi_blocks.c
	$(CC) $(CFLAGS) $(CSTD) -c gsi_blocks.c -o gsi_blocks.o

# outros

clean:
	rm -rf *.o

mrproper:clean
	rm -rf limpaGSI
